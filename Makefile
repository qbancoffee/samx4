# SAMx4

# (C) 2023 Ciaran Anscomb
#
# Released under the Creative Commons Attribution-ShareAlike 4.0
# International License (CC BY-SA 4.0).  Full text in the LICENSE file.

PACKAGE_NAME = samx4
PACKAGE_VERSION = 1.0

.PHONY: all
all:
	@echo "Only 'make dist' will do anything here, sorry."

####

distdir = $(PACKAGE_NAME)-$(PACKAGE_VERSION)

.PHONY: dist
dist:
	git archive --format=zip --prefix=$(distdir)/ -o $(distdir).zip HEAD
