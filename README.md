---
title: 'SAMx4 - MC6883 replacement board'
author:
- Ciaran Anscomb
...

Intended to replace the functionality of the SN74LS783 Synchronous Address
Multiplexer (MC6883), and extend it to provide the same functionality as
Stewart Orchard's 256K banker board.

Released under the Creative Commons Attribution-ShareAlike 4.0 International
License (CC BY-SA 4.0).  Full text in the LICENSE file.

KiCad schematic and PCB designs in this directory.

cpld/ contains VHDL source for the Xilinx XC95144XL CPLD.  Build with ISE.

Various options are configurable as boolean constants near the top of the VHDL
source, including:

- 256K banker support
- 4K/16K ram sizes
- SN74LS785 behaviour[^1]
- Fast mode video[^2]

[^1]: The SN74LS785 outputs different values for S in map type 1, and supports
    an extra 16K x 4 DRAM type.

[^2]: When RAM is not being accessed, video data can be fetched instead.  When
    running mostly from ROM, this lets you see a fairly noisy picture.  Because
    this also enables some refresh, it breaks Stewart Orchard's refresh tester.

Note that for some configuration combinations you may need to enable
"exhaustive fit" in ISE.

With thanks to Stewart Orchard for various notes on timing, and to Pedro Peña
for extensive testing across a range of Tandy Colour Computers.
